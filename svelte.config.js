import adapter from '@sveltejs/adapter-static';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	adapter: adapter({
	  // default options are shown. On some platforms
	  // these options are set automatically — see below
	  pages: 'build',
	  assets: 'build',
	  fallback: null,
	  precompress: false,
	  strict: false
	}),
	kit: {
		adapter: adapter()
	}
};

export default config;
